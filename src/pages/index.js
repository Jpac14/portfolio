import {Box, VStack} from "@chakra-ui/react"

import {Hero} from "../components/Hero"
import {Component} from "../components/Component"
import {Projects} from "../components/Projects"

export default function Home() {
  return (
    <Box bg="blue.50">
      <Hero />
      <VStack paddingTop={12} spacing={12}>
        <Component title="About 📗">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi faucibus at nisl in sodales.
          Nulla tortor risus, tristique a commodo nec, fermentum at sem. Proin vulputate euismod dui,
          in bibendum enim dictum vitae. Praesent lacinia dui quis nibh convallis iaculis. Nunc nisl
          est, imperdiet suscipit finibus eget, aliquam ut odio. In non libero et risus pellentesque
          condimentum tincidunt nec neque. Pellentesque arcu nulla, dictum nec ante non, scelerisque
          lobortis massa. Aliquam laoreet elementum pulvinar. Ut scelerisque est non elit lacinia, in
          aliquet nisl ultrices. Morbi malesuada viverra ligula a auctor. Donec auctor quam in dolor
          condimentum, quis gravida turpis aliquam.
        </Component>
        <Component title="Projects 💻">
          <Projects/>
        </Component>
      </VStack>
    </Box>
  )
}
