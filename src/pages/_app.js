import {ChakraProvider, extendTheme} from "@chakra-ui/react"

import "@fontsource/inter"
import "../styles/global.css"

const theme = extendTheme({
  fonts: {
    heading: "Inter",
    body: "Inter",
  },
})

function MyApp({Component, pageProps}) {
  return (
    <ChakraProvider theme={theme}>
      <Component {...pageProps}/>
    </ChakraProvider>
  )
}

export default MyApp
