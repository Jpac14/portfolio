import {Flex, Center, Text} from "@chakra-ui/react"

export function Component({title, children, props}) {
  return (
    <Flex w="full" {...props}>
      <Center w="20%">
        <Text bg="blue.400" p={5} borderRadius="xl" fontSize="3xl" shadow="md" color="white">
          {title}
        </Text>
      </Center>
      <Center flex={1} marginRight={8}>
        {children}
      </Center>
    </Flex>
  )
}
