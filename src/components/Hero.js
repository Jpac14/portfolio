import Particles from "react-tsparticles"

import {Box, Center, Text, VStack} from "@chakra-ui/react"

import {Navbar} from "./Navbar"

export function Hero() {
  return (
    <Box h="100vh" w="full">
      <Particles options={options} />
      <Center w="full" h="full">
        <Box
          w="3xl"
          h="xs"
          zIndex="overlay"
          border="solid"
          borderColor="white"
          bgColor="blue.400"
          borderRadius="xl"
          shadow="md"
        >
          <Center h="full">
            <VStack spacing={4}>
              <VStack>
                <Text fontSize="6xl" color="white" textDecoration="underline">
                  Hi, I'm Josh 👋
                </Text>
                <Text color="white" fontWeight="500" textAlign="center" px="12">
                  A young developer & student from Australia, who is learning new things in
                  mathematics, science, and technology. Looking to make some money.
                </Text>
              </VStack>
              <Navbar />
            </VStack>
          </Center>
        </Box>
      </Center>
    </Box>
  )
}

const options = {
  fullScreen: {enable: false},
  particles: {
    color: {
      value: "#000000",
    },
    links: {
      color: {
        value: "#000000",
      },
      enable: true,
      opacity: 0.4,
    },
    move: {
      enable: true,
      outModes: {
        bottom: "out",
        left: "out",
        right: "out",
        top: "out",
      },
      speed: 2,
    },
    number: {
      density: {
        enable: true,
      },
      value: 80,
    },
    opacity: {
      value: 0.5,
    },
    size: {
      value: {
        min: 0.1,
        max: 3,
      },
    },
  },
}
