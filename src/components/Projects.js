import {Grid} from "@chakra-ui/react"
import {ProjectCard} from "./ProjectCard"

export function Projects() {
  return (
    <Grid w="full" templateColumns="repeat(3, 1fr)" templateRows="repeat(2, 1fr)" gap={4}>
      {projects.map((project) => (
        <ProjectCard key={project.name} {...project} />
      ))}
    </Grid>
  )
}

const projects = [
  {
    name: "Portfolio",
    personal: true,
    tags: {
      NextJS: "https://nextjs.org/",
      React: "https://reactjs.org/",
      ChakraUI: "https://chakra-ui.com/",
      tsParticles: "https://particles.js.org/",
      Javascript: "https://javascript.com",
      HTML5: "https://en.wikipedia.org/wiki/HTML5",
      CSS3: "https://en.wikipedia.org/wiki/CSS",
      Yarn: "https://yarnpkg.com/",
    },
    licenses: {"GNU GPL v3": "https://www.gnu.org/licenses/gpl-3.0.en.html"},
    homepage: "https://gitlab.com/Jpac14/portfolio",
  },
  {
    name: "My OS",
    personal: true,
    tags: {
      C: "https://en.wikipedia.org/wiki/C_(programming_language)",
      Make: "https://en.wikipedia.org/wiki/Make_(software)",
      GCC: "https://gcc.gnu.org/",
      QEMU: "https://www.qemu.org/",
      Limine: "https://github.com/limine-bootloader/limine",
      Stivale2: "https://github.com/stivale/stivale",
    },
    licenses: {"GNU GPL v3": "https://www.gnu.org/licenses/gpl-3.0.en.html"},
    homepage: "https://gitlab.com/Jpac14/my-os",
  },
  {
    name: "AFL Hype Predictor",
    personal: true,
    tags: {
      Python: "https://www.python.org/",
      Juypter: "https://jupyter.org/",
      Pandas: "https://pandas.pydata.org/",
      Matplotlib: "https://matplotlib.org/",
      Statsmodels: "https://www.statsmodels.org/",
    },
    licenses: {},
    homepage:
      "https://colab.research.google.com/drive/1tBPOiUNc9EIXF5qNfVapqbU0-5BpdL69?usp=sharing",
  },
  {
    name: "Team Seas MC Server",
    personal: true,
    contributed: true,
    tags: {
      Java: "https://www.java.com/",
      Gradle: "https://gradle.org/",
      "Gitlab CI": "https://docs.gitlab.com/ee/ci/",
      PaperMC: "https://papermc.io/",
      JUnit: "https://junit.org/",
      Docker: "https://www.docker.com/",
      Redis: "https://redis.io/",
      Terraform: "https://www.terraform.io/",
    },
    licenses: {
      "GNU AGPL v3": "https://www.gnu.org/licenses/agpl-3.0.en.html",
      "GNU GPL v3": "https://www.gnu.org/licenses/gpl-3.0.en.html",
    },
    homepage: "https://gitlab.com/teamseas-mc-server",
  },
  {
    name: "OpenStreetCraft Minecraft",
    contributed: true,
    tags: {},
    licenses: {"GNU AGPL v3": "https://www.gnu.org/licenses/agpl-3.0.en.html"},
    homepage: "https://gitlab.com/openstreetcraft/minecraft",
  },
  {
    name: "OpenStreetCraft Website",
    contributed: true,
    tags: {
      NextJS: "https://nextjs.org/",
      React: "https://reactjs.org/",
      ChakraUI: "https://chakra-ui.com/",
      Javascript: "https://javascript.com",
      HTML5: "https://en.wikipedia.org/wiki/HTML5",
      CSS3: "https://en.wikipedia.org/wiki/CSS",
      PNPM: "https://pnpm.js.org/",
      TailwindCSS: "https://tailwindcss.com/",
      Axios: "https://axios-http.com/",
    },
    licenses: {"Creative Commons": "https://creativecommons.org/licenses/by-sa/4.0/"},
    homepage: "https://gitlab.com/openstreetcraft/openstreetcraft.gitlab.io",
  },
]
