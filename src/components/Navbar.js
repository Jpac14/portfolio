import {Flex, Button, HStack, Box, Center} from "@chakra-ui/react"

export function Navbar(props) {
  return (
    <Flex w="full" zIndex="overlay" justifyContent="center" {...props}>
      <Box borderRadius="md">
        <Center>
          <HStack>
            <Button>About 📗</Button>
            <Button>Projects 💻</Button>
            <Button>Skills 🔥</Button>
            <Button>Goals 🎯</Button>
            <Button as="a" href="mailto:jpac14@outlook.com">
              Contact ✉️
            </Button>
            <Button
              as="a"
              target="_blank"
              href="https://docs.google.com/document/d/1ga1Lx1eo62SVZe2PTjxEnRFKXhfI3DhGdvvvHzrB4HU/edit?usp=sharing"
            >
              Resume 📝
            </Button>
          </HStack>
        </Center>
      </Box>
    </Flex>
  )
}
