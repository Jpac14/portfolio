import {Center, Heading, Box, Flex, Badge, Link} from "@chakra-ui/react"

export function ProjectCard({name, tags, licenses, personal, contributed, homepage, ...props}) {
  return (
    <Box
      bg="white"
      h="52"
      borderWidth="1px"
      rounded="lg"
      shadow="lg"
      _hover={{transform: "scale(1.05)", transition: "transform .2s"}}
      
      {...props}
      as="a"
      href={homepage}
    >
      <Center w="100%" h="100%">
        <Box textAlign="center">
          <Heading px={4} py={1} fontSize="3xl">
            {name}
          </Heading>
          <Flex px={4} pb={1} flexWrap="wrap" justifyContent="center">
            {Object.entries(tags).map(([key, value]) => (
              <Badge key={key} rounded="full" px={2} mr={1} mb={1} colorScheme="green">
                <Link href={value}>{key}</Link>
              </Badge>
            ))}
            {Object.entries(licenses).map(([key, value]) => (
              <Badge key={key} rounded="full" px={2} mr={1} mb={1} colorScheme="yellow">
                <Link href={value}>{key}</Link>
              </Badge>
            ))}
            {personal && (
              <Badge rounded="full" px={2} mr={1} mb={1} colorScheme="red">
                Personal
              </Badge>
            )}
            {contributed && (
              <Badge rounded="full" px={2} mr={1} mb={1} colorScheme="red">
                Contributed
              </Badge>
            )}
          </Flex>
        </Box>
      </Center>
    </Box>
  )
}
